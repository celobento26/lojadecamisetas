Rails.application.routes.draw do

  resources :departamentos
  root "produtos#index"

  resources :usuarios

  #get "produtos" => "produtos#index"
  #get "produtos/hello" => "produtos#hello"
  #get "/produtos/new" => "produtos#new"
  #delete "/produtos/:id" => "produtos#destroy", as: :produto
  #post "/produtos" => "produtos#create"
  resources :produtos, only: [:new, :create, :destroy, :edit, :update]
  get "/produtos/busca" => "produtos#busca", as: :busca_produto

  get "usuarios" => "usuarios#index"

end
