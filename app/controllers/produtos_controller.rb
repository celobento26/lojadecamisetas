class ProdutosController < ApplicationController

  before_action :set_produto, only: [:edit, :update, :destroy]

  def hello
    @message = "Marcelo Bento Ferreira"
  end

  def index
    @produtos = Produto.all.order(descricao: :desc).limit(20)
  end

  def busca
      @nome_a_buscar = params[:nome]
      @produtos_buscado = Produto.where "nome like ?", "%#{@nome_a_buscar}%"

  end

  def edit
    renderiza_edit
  end

  def update
    if @produto.update produto_params
      flash[:notice] = "Registro atualizado com sucesso"
      redirect_to root_url
    else
      render :edit
    end
  end



  def new
          @produto = Produto.new
          @departamentos = Departamento.all
  end

  def create
    @produto = Produto.new produto_params
    if @produto.save
        flash[:notice] = "Criado com sucesso!"
        redirect_to root_url
    else
        render :new
    end

  end

  def destroy
    @produto.destroy
    redirect_to root_url
  end

  private

  def set_produto
    id = params[:id]
    @produto = Produto.find(id)
  end

  def renderiza_new
    @departamentos = Departamento.all
    render :new
  end

  def renderiza_edit
    @departamentos = Departamento.all
    renderiza :edit
  end

  def renderiza(view)
    render view
  end

  def produto_params
    params.require(:produto).permit(:nome, :preco, :descricao, :quantidade, :departamento_id)
  end

end
