class Usuario < ActiveRecord::Base
  validates_presence_of :dsc_senha, :dsc_login
  validates_confirmation_of  :dsc_senha
  validates_length_of :dsc_login, minimum: 8, allow_blank: false
  validates_length_of :dsc_login, in: 5..10
  
end
