class Perfil < ActiveRecord::Base
  validates_presence_of :role, :nome, :status
  validates :descricao, presence: true
end
