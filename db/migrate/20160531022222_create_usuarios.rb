class CreateUsuarios < ActiveRecord::Migration
  def change
    create_table :usuarios do |t|
      t.integer :isn_usuario
      t.string :dsc_login
      t.string :dsc_senha

      t.timestamps null: false
    end
  end
end
