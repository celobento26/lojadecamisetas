class CreatePerfils < ActiveRecord::Migration
  def change
    create_table :perfils do |t|
      t.string :nome
      t.string :role
      t.text :descricao
      t.boolean :status

      t.timestamps null: false

      t.index :role, unique: true
    end
  end
end
